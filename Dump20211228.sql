-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: steven_white
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `award_item`
--

DROP TABLE IF EXISTS `award_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `award_item` (
  `itemID` int unsigned NOT NULL AUTO_INCREMENT,
  `awardName` varchar(255) NOT NULL,
  `year` varchar(20) NOT NULL,
  `description` varchar(500) NOT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `award_item`
--

LOCK TABLES `award_item` WRITE;
/*!40000 ALTER TABLE `award_item` DISABLE KEYS */;
INSERT INTO `award_item` VALUES (1,'Advanced Communicator Bronze Award, Toastmasters','2015','Completed two advanced communication manuals (10 speeches in total). Topics focused on speaking to inform and speaking humorously.'),(2,'Vice President\'s List, MUN','2010-13','Academically within the top 10% of students in the faculty of science.'),(3,'Competent Communicator Award, Toastmasters','2012','Completed 10 public speech presentations from the competent communication manual.'),(4,'The Governor General\'s Academic Medal (Bronze)','2009','Highest average upon graduation of highschool.');
/*!40000 ALTER TABLE `award_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edu_bullet_point`
--

DROP TABLE IF EXISTS `edu_bullet_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `edu_bullet_point` (
  `pointID` int unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int unsigned DEFAULT NULL,
  `point` text NOT NULL,
  PRIMARY KEY (`pointID`),
  KEY `itemID` (`itemID`),
  CONSTRAINT `edu_bullet_point_ibfk_1` FOREIGN KEY (`itemID`) REFERENCES `education_item` (`itemID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edu_bullet_point`
--

LOCK TABLES `edu_bullet_point` WRITE;
/*!40000 ALTER TABLE `edu_bullet_point` DISABLE KEYS */;
INSERT INTO `edu_bullet_point` VALUES (1,2,'Learned more advanced concepts such as strategic points of tension, energy transfers, tempo changes, progressions for tuck balance training, and using animal locomotion in flows.'),(2,2,'Passed the test-outs for demonstration of the movements and instructing a student.'),(3,3,'Learned the first set of yang style tai chi comprising 21 movements.'),(4,3,'Other course content included: breathing technique, alignment, the concept of empty and full, five directions, health benefits and history of tai chi chuan.'),(5,4,'Completed a level 1 workshop on the six components of animal flow: wrist mobilizations, activations, form specific stretches, traveling forms, switches/transitions, and flows.'),(6,4,'Passed the test-outs for demonstration of the movements and instructing a student.'),(7,5,'Studied in Hatha and Vinyasa Yoga: 100 hrs techniques; 25 hrs teaching methodology; 20 hrs anatomy and physiology; 30 hrs philosophy, ethics and lifestyle; and 25 hrs practicum.');
/*!40000 ALTER TABLE `edu_bullet_point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education_item`
--

DROP TABLE IF EXISTS `education_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `education_item` (
  `itemID` int unsigned NOT NULL AUTO_INCREMENT,
  `credentialName` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `dateRange` varchar(255) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `altText` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education_item`
--

LOCK TABLES `education_item` WRITE;
/*!40000 ALTER TABLE `education_item` DISABLE KEYS */;
INSERT INTO `education_item` VALUES (1,'Certificate in Full-Stack Web Development','York University School of Continuing Studies - Toronto, ON','January - December 2021',NULL,NULL),(2,'Certified Animal Flow Level 2 Instructor','DTS Fitness Education - Toronto, ON','November 2019','./resumeImages/AFlv2Certificate.jpg','Animal Flow level 2 certificate'),(3,'Certified Tai Chi Chuan Level 1 Instructor','Wu Xing Martial Arts - Toronto, ON','October - December 2018','./resumeImages/TaiChilv1Certificate.jpg','Tai Chi level 1 certificate'),(4,'Certified Animal Flow Level 1 Instructor','DTS Fitness Education - Toronto, ON','December 2017','./resumeImages/AFlv1Certificate.jpg','Animal Flow level 1 certificate'),(5,'200-hour Yoga Teacher Training','Yogaspace - Toronto, ON','July - August 2017','./resumeImages/yoga200hrCertificate.jpg','200 hour yoga certificate'),(6,'Advanced Diploma in Chemical Engineering Technology (fast-track program)','Durham College - Oshawa, ON; GPA: 5/5','September 2014 - April 2015','./resumeImages/CETdiploma.JPG','Chemical Engineering Technology advanced diploma'),(7,'B.Sc. Hons. in Environmental Science (Chemistry)','Memorial University of Newfoundland (MUN), Grenfell Campus - Corner Brook, NL; GPA: 3.94/4.00','September 2009 - April 2013','./resumeImages/BScHonsDegree.jpg','Bachelor of Science degree'),(8,'High School Diploma','Crescent Collegiate - South Dildo, Trinity Bay, NL','2003-09','./resumeImages/highSchoolDiploma.jpg','Highschool diploma');
/*!40000 ALTER TABLE `education_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_bullet_point`
--

DROP TABLE IF EXISTS `emp_bullet_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emp_bullet_point` (
  `pointID` int unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int unsigned DEFAULT NULL,
  `point` text NOT NULL,
  PRIMARY KEY (`pointID`),
  KEY `itemID` (`itemID`),
  CONSTRAINT `emp_bullet_point_ibfk_1` FOREIGN KEY (`itemID`) REFERENCES `employment_item` (`itemID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_bullet_point`
--

LOCK TABLES `emp_bullet_point` WRITE;
/*!40000 ALTER TABLE `emp_bullet_point` DISABLE KEYS */;
INSERT INTO `emp_bullet_point` VALUES (1,1,'Used MindBody Online software to manage sales, classes, appointments, and client accounts.'),(2,1,'Managed inventory, including: ordering more retail products as needed, using CRM to enter product sales, and using Excel to track inventory levels.'),(3,1,'Improved the organization of retail inventory and studio props.'),(4,1,'Improved the studio\'s private client waitlist system by using a combination of client types and mailing lists in MindBody.'),(5,1,'Helped with editing auto-emails set up in MindBody and looking for areas of improvement.'),(6,1,'Managed daily emails and phone correspondence.'),(7,1,'Greeted clients and assisted them with studio related inquiries.'),(8,2,'Used MindBody Online and Wellness Living software to manage inventory, sales, classes, and client accounts.'),(9,2,'Responsible for backend setup of items in Wellness Living including but not limited to: automated emails, pricing options, client types, automated discounts, classes and events, and instructor profiles.'),(10,2,'Pulled reports from Wellness Living including reports for class attendance, class averages, unpaid visits, first visits, and sales.'),(11,2,'Used Squarespace to make changes on the studio’s website.'),(12,2,'Significantly reduced the occurrence of unpaid visits from monthly numbers in double-digits to single-digits. Furthermore, on an ongoing basis, provided help with reconciling any remaining unpaid visits to reduce loss of income for the studio.'),(13,2,'Responsible for organizing instructors\' payroll on a bimonthly basis and sending a summary to the studio owner for payment. Helped improve the process by setting up Wellness Living to track payroll which reduced the workload for all staff involved.'),(14,2,'Provided training to energy exchange workers. Created \'how to\' guides and video tutorials for reference.'),(15,2,'General cleaning, organization, and administrative duties.'),(16,3,'Taught a combined total of about 100 hours in group yoga classes, mainly in the vinyasa style.'),(17,3,'Taught a handful of private one-on-one yoga sessions.'),(18,4,'Responsible for manufacturing of chemicals according to predefined procedures and within health and safety protocols.'),(19,4,'Member of safety training and education team.'),(20,4,'Worked in an ethnically diverse environment.'),(21,4,'Communicated work in progress between shifts.'),(22,4,'Ensured all documentation related to production is current, complete and accurate.'),(23,4,'Followed inventory and waste disposal procedures to optimize production efficiency.'),(24,5,'Worked in a team environment to provide quality service.'),(25,5,'Cleaned various parts of the restaurant to ensure cleanliness and to meet safety standards.'),(26,5,'Worked on the cash register to process cash, debit, and credit transactions.');
/*!40000 ALTER TABLE `emp_bullet_point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employment_item`
--

DROP TABLE IF EXISTS `employment_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employment_item` (
  `itemID` int unsigned NOT NULL AUTO_INCREMENT,
  `jobPosition` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `dateRange` varchar(255) NOT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employment_item`
--

LOCK TABLES `employment_item` WRITE;
/*!40000 ALTER TABLE `employment_item` DISABLE KEYS */;
INSERT INTO `employment_item` VALUES (1,'Studio Coordinator (part-time)','Merrithew - Toronto, ON','May 2018 - March 2020'),(2,'Studio Coordinator (part-time)','Pur Yoga - Toronto, ON','November 2017 - February 2020'),(3,'Yoga Teacher','Yogaspace, Pur Yoga, Leslieville Yoga Sanctuary - Toronto, ON','August 2017 - August 2020'),(4,'Production Chemist','Digital Specialty Chemicals - Scarborough, ON','August 2016 - June 2017'),(5,'Cashier/Team Member','Tim Horton\'s - Whitbourne, NL','January - July, 2016');
/*!40000 ALTER TABLE `employment_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `messageID` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phoneNumber` char(10) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`messageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES ('c0578090-188f-4966-91a9-c21e75077ded','Steven White','example@gmail.com','7091112222','this is a test');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_item`
--

DROP TABLE IF EXISTS `portfolio_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolio_item` (
  `itemID` int unsigned NOT NULL AUTO_INCREMENT,
  `sectionID` int unsigned NOT NULL,
  `itemDescription` mediumtext NOT NULL,
  `itemTitle` varchar(100) NOT NULL,
  `codeLink` varchar(255) DEFAULT NULL,
  `demoLink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`itemID`),
  KEY `sectionID` (`sectionID`),
  CONSTRAINT `portfolio_item_ibfk_1` FOREIGN KEY (`sectionID`) REFERENCES `portfolio_section` (`sectionID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_item`
--

LOCK TABLES `portfolio_item` WRITE;
/*!40000 ALTER TABLE `portfolio_item` DISABLE KEYS */;
INSERT INTO `portfolio_item` VALUES (1,1,'Exercise: use HTML5 to create a table and contact form, and use CSS3 to style the table.','Basic Table and Contact Form','','./portfolioLinks/CSSday2Exercise.html'),(2,1,'Exercise: use JavaScript to create a calculator that calculates the area of a rectangle by allowing user to enter width and height.','Rectangle Area Calculator','','./portfolioLinks/JSday3AreaCalculation.html'),(3,2,'Course project of creating a backend application for a \'contact us\' form by using NodeJS.','Backend for Contact Form','https://gitlab.com/steve-dave8/fs1020-winter2021-course-project',''),(4,3,'Assignment: create a webpage based on a mockup.','Webpage Based On Mockup','https://github.com/steve-dave8/fs1010--assignment-1',''),(5,3,'Assignment: create a basic todo app using jQuery.','jQuery Todo App','https://github.com/steve-dave8/fs1010-jQuery-todo-app',''),(6,3,'Exercise: add painting functionality to provided canvas by using jQuery.','jQuery Painting Canvas','https://github.com/steve-dave8/jQuery-Paint-Canvas',''),(7,4,'Group project: create an electronic medical record (EMR) system using ReactJS, NodeJS, and MySQL. Our project was divided into 3 repositories for frontend, backend, and database. See the final result in our GitLab group.','EMR System','https://gitlab.com/york-u-s21-fs1030-group-d-course-project',''),(8,7,'A twist on typical fortune cookies, this is a static webpage that was deployed using AWS.','Misfortune Cookies','https://github.com/steve-dave8/Misfortune-Cookies','https://misfortune-cookies.steven-white.me/'),(9,7,'An app, created using React JS, for designing Animal Flow sequences. Features include: over 100 movements, for a selected movement a list of next possible movements will be provided from which you can pick your next move and repeat the process to build a flow, options to filter next moves by level and/or component, generate random flows, mirror your flows (swapping left/right).\n','Animal Flow Sequence Builder','https://github.com/steve-dave8/Animal-Flow-Sequence-Builder',NULL),(10,7,'A client-side, single-page application using HTML5 Canvas to apply filters to an incoming webcam video stream and record the filtered video. Created with React JS and deployed with AWS.','Webcam Filters','https://gitlab.com/steve-dave8/webcam-filters','https://webcam-filters.steven-white.me/');
/*!40000 ALTER TABLE `portfolio_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_section`
--

DROP TABLE IF EXISTS `portfolio_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolio_section` (
  `sectionID` int unsigned NOT NULL AUTO_INCREMENT,
  `sectionTitle` varchar(255) NOT NULL,
  `sectionDescription` mediumtext,
  `order` int unsigned NOT NULL,
  PRIMARY KEY (`sectionID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_section`
--

LOCK TABLES `portfolio_section` WRITE;
/*!40000 ALTER TABLE `portfolio_section` DISABLE KEYS */;
INSERT INTO `portfolio_section` VALUES (1,'CSFS 1000 - Fundamentals of Full-Stack Web Development','Course description: an introduction to the technology that makes the web run, the tools and terminologies involved, and the fundamentals of each technology. The importance of design, type, security, privacy, usability and accessibility are examined through the components of visual and text communication.',5),(2,'CSFS 1020 - Server Side Programming and Tools','Course description: learn how to integrate business logic into web applications by building and coding a powerful backend using the appropriate technologies. The tools used include: NodeJS, ExpressJS, npm, nodemon, and Postman. Also, develop knowledge and skills in the areas of user authentication and security considerations for web applications.',4),(3,'CSFS 1010 - Web UI Concept and Frameworks','Course description: learn how to build effective, interactive, and responsive websites by using languages including HTML, CSS and JavaScript. The tools used include Bootstrap, jQuery, and ReactJS.',3),(4,'CSFS 1030 - Database Design and Principles','Course description: learn about the fundamentals, architecture, model, design, security, optimization and implementation of databases - Relational and NoSQL - in real world applications. Build the skills to design and integrate any database with front-end and back-end applications.',2),(7,'Personal Projects','',1);
/*!40000 ALTER TABLE `portfolio_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `userID` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Sample User','$2a$10$3OKykAVi6TOPnl8ek/6xf.ThFmV1UGqJ/9E2c2pf9slXeOIx1V5Qa','sample-user@placeholder.com'),(3,'Steven White','$2a$10$r.LCb2b/kRF28.g.eK9YfehQY7MZyUbkv6gNYX3DZbmfhAJ6xOidm','steven_green_white@hotmail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-28 15:10:40
